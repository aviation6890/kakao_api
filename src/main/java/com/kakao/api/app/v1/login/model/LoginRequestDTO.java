package com.kakao.api.app.v1.login.model;

import lombok.Data;

@Data
public class LoginRequestDTO {
    private String client_id;
    private String redirect_uri;
    private String response_type = "code";
    private String state;
    private String Cross;
    private String prompt;
}
