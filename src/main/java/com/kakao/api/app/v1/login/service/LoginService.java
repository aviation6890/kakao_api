package com.kakao.api.app.v1.login.service;

import com.kakao.api.app.v1.login.model.LoginResponseDTO;
import org.springframework.stereotype.Service;

@Service
public interface LoginService {
    LoginResponseDTO getKakaoToken(String code);

    String getKakaoAuth();
}
