package com.kakao.api.app.v1.login.controller;

import com.kakao.api.app.v1.login.model.LoginResponseDTO;
import com.kakao.api.app.v1.login.service.LoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@Slf4j
public class LoginController {

    private final LoginService loginService;

    @GetMapping("/v1/login")
    public String login(Model model) {

        model.addAttribute("loginURL", loginService.getKakaoAuth());
        return "user/login";
    }

    @GetMapping("/redirectMap")
    public String kakao_login(@RequestParam String code) {

        LoginResponseDTO kakaoToken = loginService.getKakaoToken(code);
        log.info("kakaoToken::{}", kakaoToken);
        return "user/result";
    }

}
