package com.kakao.api.app.v1.login.model;

import lombok.Data;

@Data
public class LoginResponseDTO {
    private String code;
    private String state;
    private String error;
    private String error_description;
}
