package com.kakao.api.app.v1.login.service.impl;

import com.google.gson.Gson;
import com.kakao.api.app.v1.login.model.LoginResponseDTO;
import com.kakao.api.app.v1.login.service.LoginService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final Environment environment;
    private RestTemplate restTemplate;
    private final Gson gson;

    @Value("${spring.social.kakao.api.rest}")
    private String kakaoClientId;

    @Value("${spring.social.kakao.api.redirect}")
    private String kakaoRedirect;


    @Override
    public String getKakaoAuth() {
        StringBuilder loginURL = new StringBuilder();
        loginURL.append(environment.getProperty("spring.social.kakao.url.login"));
        loginURL.append("?client_id=").append(kakaoClientId);
        loginURL.append("&response_type=code");
        loginURL.append("&redirect_uri=").append(kakaoRedirect);
        log.info("loginURL::{}",loginURL.toString());

        //https://kauth.kakao.com/oauth/authorize?client_id=5a93fc6a3b19cadef49da72d695cd9b4&response_type=code&redirect_uri=http://localhost:8080/redirectMap

        return loginURL.toString();
    }

    @Override
    public LoginResponseDTO getKakaoToken(String code) {
        RestTemplate restTemplate = new RestTemplate();
        // http header
        HttpHeaders httpHeaders = new HttpHeaders();
        // Content-type: application/x-www-form-urlencoded
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        // parameters
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "authorization_code");
        params.add("client_id", kakaoClientId);
        params.add("redirect_uri", kakaoRedirect);
        params.add("code", code);

        // http entity
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, httpHeaders);

        ResponseEntity<String> response = restTemplate.postForEntity(environment.getProperty("spring.social.kakao.url.token"), request, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            return gson.fromJson(response.getBody(), LoginResponseDTO.class);
        }
        return null;
    }

}
